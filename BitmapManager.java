

import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

public class BitmapManager {

	public static HashMap<String, Bitmap> BITMAPS = new HashMap<String, Bitmap>();
	public static HashMap<String, double[]> GLES_BMP_SIZE_RATIOS = new HashMap<String, double[]>();
	public static void addBitmap(String name, int resourceid, Context c) {
		Bitmap b = BitmapFactory.decodeResource(c.getResources(), resourceid);
		BITMAPS.put(name, b);
	}

	public static void addBitmap(String name, int resourceid, Context c, int dsrdwidth, int dsrdheight) {
		try {
			Bitmap b = BitmapFactory.decodeResource(c.getResources(), resourceid);
			b = Bitmap.createScaledBitmap(b, dsrdwidth, dsrdheight, true);
			BITMAPS.put(name, b);
		} catch (Exception e) {
			Log.e("", "addBitmap: " + e.getMessage());
		}
	}
	public static void addBitmapForGLES(String name, int resourceid, Context c, int dsrdwidth, int dsrdheight) {
		try {
			int ow = dsrdwidth;
			int ih = dsrdheight;
			Log.e("","Creating bitmap for GLES:  '" + name + "'");
			Bitmap b = BitmapFactory.decodeResource(c.getResources(), resourceid);
			b = Bitmap.createScaledBitmap(b, dsrdwidth, dsrdheight, true);
			dsrdwidth = Math.abs(dsrdwidth);
			dsrdheight = Math.abs(dsrdheight);
			int thew=1;
			int theh = 1;
			int p = 0;
			Log.e("", "Okay");
			while(thew<dsrdwidth && p < 20){
				thew = thew * 2;
				p++;
			}
			p=0;
			while(theh<dsrdheight && p < 20){
				theh = theh * 2;
			}
			double ratiow = (double) dsrdwidth / (double) thew;
			double ratioh = (double) dsrdheight / (double) theh;
			GLES_BMP_SIZE_RATIOS.put(name, new double[]{ratiow, ratioh});
			Log.e("", "New bitmap: " + thew + "; " + theh + " | ratios: " + ratiow + "; " + ratioh);
			Bitmap toreturn  = Bitmap.createBitmap(thew, theh, Config.ARGB_8888);
			Canvas canvas = new Canvas(toreturn);
			if(ow<0){
				canvas.scale(-1, 1);
				canvas.translate(ow, 0);
			}
			canvas.drawBitmap(b, 0, 0, new Paint());
			BITMAPS.put(name, toreturn);
		} catch (Exception e) {
			Log.e("", "addBitmap: " + e.getMessage());
		}
	}

	public static Bitmap getBitmap(String name) {
		try {
			return BITMAPS.get(name);
		} catch (Exception e) {
			return Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
		}
	}

}
