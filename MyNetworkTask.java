

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

public class MyNetworkTask extends AsyncTask<String, Integer, Integer> {

	public AsyncTaskResponseDispatcher dispatcher;

	public MyNetworkTask(AsyncTaskResponseDispatcher d) {
		dispatcher = d;
	}

	public Integer doInBackground(String... urls) {
		int count = urls.length;
		int complete = 0;
		for (int i = 0; i < count; i++) {
			String s = getResponse(urls[i]);
			dispatcher.onResponseReturned(s);
			// Escape early if cancel() is called
			if (isCancelled())
				break;
		}
		complete = 1;
		return complete;
	}

	protected void onProgressUpdate(Integer... progress) {
		Log.e("", "onProgressUpdate: " + progress);
		// setProgressPercent(progress[0]);
	}

	protected void onPostExecute(Integer result) {
		// showDialog("Downloaded " + result + " bytes");
		Log.e("", "onPostExecute: " + result);
	}

	@SuppressLint("NewApi")
	public static String getResponse(String url) {
		String r = "";
		try {
			if (android.os.Build.VERSION.SDK_INT > 9) {
				// StrictMode.ThreadPolicy policy = new
				// StrictMode.ThreadPolicy.Builder().permitAll().build();
				// StrictMode.setThreadPolicy(policy);
			}
			Log.e("", "Getting response from: '" + url + "'");
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			Log.e("", "Got response");
			r = convertResponseToString(response);
			Log.e("", "Converted response");
		} catch (Exception e) {
			Log.e("", "Could not get response: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
		return r;
	}
	public static String convertResponseToString(HttpResponse response) throws IllegalStateException, IOException {
		InputStream inputStream;
		String res = "";
		try {
			StringBuffer buffer = new StringBuffer();
			inputStream = response.getEntity().getContent();
			int contentLength = (int) response.getEntity().getContentLength();
			if (contentLength < 0) {
			} else {
				byte[] data = new byte[512];
				int len = 0;
				try {
					while (-1 != (len = inputStream.read(data))) {
						buffer.append(new String(data, 0, len));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				res = buffer.toString();
			}
		} catch (Exception e) {
			Log.e("", "Error converting response");
		}
		return res;
	}
}
