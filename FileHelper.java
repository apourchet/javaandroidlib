

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;

public class FileHelper {
	
	public static boolean writeFile(Context c, String filename, String file) {
		try {
			OutputStreamWriter out = new OutputStreamWriter(c.openFileOutput(filename, 0));
			out.write(file);
			out.close();
			return true;
		} catch (Exception e) {
			Log.e("", "Error writing file: " + e.getMessage());
			return false;
		}
	}
	public static String readFileFromAssets(Context c, String filename){
		String data = "";
		try {
			InputStream instream = c.getAssets().open(filename);
			if (instream != null) {
				InputStreamReader inputreader = new InputStreamReader(instream);
				BufferedReader buffreader = new BufferedReader(inputreader);
				String line;
				while ((line = buffreader.readLine()) != null) {
					data += line + "";
				}
			} else {
				return null;
			}
			instream.close();
			return data.replaceAll("\n", "").replace("\r", "").replace("\t", "");
		} catch (Exception e) {
			Log.e("", "ERROR reading file: " + e.getMessage());
			return null;
		}
	}
	public static String readFile(Context c, String filename) {
		String data = "";
		try {
			InputStream instream = c.openFileInput(filename);
			if (instream != null) {
				InputStreamReader inputreader = new InputStreamReader(instream);
				BufferedReader buffreader = new BufferedReader(inputreader);
				String line;
				while ((line = buffreader.readLine()) != null) {
					data += line + "";
				}
			} else {
				return null;
			}
			instream.close();
			return data;
		} catch (Exception e) {
			Log.e("", "ERROR reading file: " + e.getMessage());
			return null;
		}
	}

	public static void deleteAllFiles(Context c) {
		for (String s : c.fileList()) {
			c.deleteFile(s);
		}
	}

	public static boolean fileExists(Context c, String filename) {
		for (int i = 0; i < c.fileList().length; i++) {
			if (filename.equals(c.fileList()[i])) {
				return true;
			}
		}
		return false;
	}

	public static void logListFiles(Context c) {
		for (int i = 0; i < c.fileList().length; i++) {
			Log.e("", "File found: " + c.fileList()[i]);
		}
	}

}
