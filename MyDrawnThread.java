

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

public abstract class MyDrawnThread extends MyThread {

	private static final String tag = "MyDrawnThread";
	public SurfaceHolder HOLDER;
	public int WIDTH, HEIGHT;
	public Context CONTEXT;
	public boolean toResume = false;
	private boolean PAUSED = false;

	public abstract void doDraw(Canvas c);

	public abstract void update();

	public MyDrawnThread(SurfaceHolder holder, Context c, int width, int height) {
		HOLDER = holder;
		CONTEXT = c;
		WIDTH = width;
		HEIGHT = height;
	}
	
	public void pauseUpdateDrawThread(){
		PAUSED = true;
	}
	public void resumeUpdateDrawThread(){
		toResume = true;
	}
	public boolean isPaused(){
		return PAUSED;
	}
	@Override
	public void run() {
		Log.i(tag, "Run");
		RUNNING = true;
		while (RUNNING) {
			if(PAUSED == true && toResume){
				toResume = false;
				PAUSED = false;
			}
			if(PAUSED == true){
				try{
					sleep(200);
				}catch(Exception e){
					
				}
			}else{
				Canvas c = null;
				try {
					c = HOLDER.lockCanvas(null);
					synchronized (HOLDER) {
						update();
						doDraw(c);
					}
				} catch (Exception e) {
					Log.e("", "Error: " + e.getMessage());
				} finally {
					if (c != null) {
						HOLDER.unlockCanvasAndPost(c);
					}
				}
			}
		}
	}

}
