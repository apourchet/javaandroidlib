

import android.util.Log;

public class TimerTeller {

	
	private static final String tag = "TimerTeller";
	private static long START_TIME = System.currentTimeMillis();
	private static long FILTER_TIME;
	private static long ALWAYS_TELL_THRESHOLD = 100000;
	
	public static void filter(int time){
		FILTER_TIME = time;
	}
	public static void setThreshold(int time){
		ALWAYS_TELL_THRESHOLD = time;
	}
	public static void startTime(){
		START_TIME = System.currentTimeMillis();
	}
	public static void stopAndTellTimer(){
		long t = System.currentTimeMillis()-START_TIME;
		if(t >= FILTER_TIME  || t > ALWAYS_TELL_THRESHOLD){
			Log.i(tag, ""+ t + " ms");
		}
	}
	
}
