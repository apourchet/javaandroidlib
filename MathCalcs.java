

public class MathCalcs {

	public static float toRadians(float angle) {
		return (float) (angle * Math.PI / 180);
	}

	public static float toDegrees(float angle) {
		return (float) (angle * 180 / Math.PI);
	}

	public static float dotProduct(float[] a, float[] b, int length) {
		float runningSum = 0;
		for (int index = 0; index < length; index++) {
			runningSum += a[index] * b[index];
		}
		return runningSum;
	}

	public static final int signOf(float f){
		if(f<0){
			return -1;
		}else{
			return 1;
		}
	}
	public static boolean isPointInPoly(int nvert, float[] vertx, float[] verty, float[] test) {
		int i, j;
		boolean c = false;
		for (i = 0, j = nvert - 1; i < nvert; j = i++) {
			if (((verty[i] > test[1]) != (verty[j] > test[1])) && (test[0] < (vertx[j] - vertx[i]) * (test[1] - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
				c = !c;
		}
		return c;
	}

	public static float[] crossProduct(float[] u, float[] v) {
		float[] w = new float[3];
		w[1] = u[2] * v[3] - u[3] * v[2];
		w[2] = u[3] * v[1] - u[1] * v[3];
		w[3] = u[1] * v[2] - u[2] * v[1];
		return w;
	}

	public static float slope(float[] a, float[] b) {
		float slope;
		if (b[0] - a[0] != 0) {
			slope = (float) (b[1] - a[1]) / (b[0] - a[0]);
			//Log.e("Slope", "Slope: " + (b[1] - a[1]) + "/" + (b[0] - a[0]));
		} else {
			slope = (float) 100000;
		}
		return slope;
	}
	public static float slope(int[] a, int[] b) {
		float slope;
		if (b[0] - a[0] != 0) {
			slope = (float) (b[1] - a[1]) / (b[0] - a[0]);
			//Log.e("Slope", "Slope: " + (b[1] - a[1]) + "/" + (b[0] - a[0]));
		} else {
			slope = (float) 100000;
		}
		return slope;
	}

	public static float yintercept(float slope, float[] a) {
		float yintercept = a[1] - slope * a[0];
		return yintercept;
	}

	public static float[] getIntersectionLines(float[] a, float[] b, float[] x, float[] y) {
		float slopec = slope(a, b);
		float slopez = slope(x, y);

		float yinterceptc = yintercept(slopec, a);
		float yinterceptz = yintercept(slopez, x);

		//slopec*X + yinterceptc = slopez*X + yinterceptz
		//X (slopec-slopez) = yinterceptz-yinterceptc
		//X = (yinterceptz-yinterceptc)/(slopec-slopez);
		//Y = X*slopec+yinterceptc
		float X = 0, Y = 0;
		if ((slopec - slopez) != 0) {
			X = (yinterceptz - yinterceptc) / (slopec - slopez);
			Y = X * slopec + yinterceptc;
			return new float[] { X, Y };
		} else {
			return null;
		}
	}

	public static float getDistanceSquared(float[] a, float[] b) {
		float dimention = a.length;
		if (a.length > b.length) {
			dimention = b.length;
		}
		float d = 0;
		for (int i = 0; i < dimention; i++) {
			d += (a[i] - b[i]) * (a[i] - b[i]);
		}
		//float d2 = (a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]) + (a[2]-b[2])*(a[2]-b[2]);
		return d;
	}
	public static float getDistanceSquared(int[] a, int[] b) {
		float dimention = a.length;
		if (a.length > b.length) {
			dimention = b.length;
		}
		float d = 0;
		for (int i = 0; i < dimention; i++) {
			d += (a[i] - b[i]) * (a[i] - b[i]);
		}
		//float d2 = (a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]) + (a[2]-b[2])*(a[2]-b[2]);
		return d;
	}

	//public static float getDistanceSquared2(float[] a, float[] b){
	//	return ((a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]) + (a[2]-b[2])*(a[2]-b[2]));
	//}

	public static boolean isPointOnSegment(float[] a, float[] b, float[] point) {
		if (a == null || b == null || point == null) {
			return false;
		}
		float[] median = new float[] { (b[0] - a[0]) / 2 + a[0], (b[1] - a[1]) / 2 + a[1], (b[2] - a[2]) / 2 + a[2] };
		//float[] medianvec = new float[]{(b[0] - a[0]) / 2, (b[1] - a[1]) / 2, (b[2] - a[2]) / 2};
		float radius = getDistanceSquared(median, b);
		if (getDistanceSquared(median, point) > radius) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean areFloatsEqual(float[] a, float[] b, float precision) {
		float diff0 = Math.abs(a[0] - b[0]);
		float diff1 = Math.abs(a[1] - b[1]);
		float diff2 = Math.abs(a[2] - b[2]);
		float avg = (diff1 + diff2 + diff0) / 3;
		if (avg < precision) {
			return true;
		}
		//System.out.println("MathCalcs.areFloatsEqual: " + avg);
		return false;
	}
/**
 * Puts the values of array b into array a. Works only if both lengths are equal.
 * @param a-array pointer to be changed
 * @param b-array pointer to be copied
 * */
	public static void setFloatArray(float[] a, float[] b) {
		if (a.length == b.length) {
			for (int i = 0; i < a.length; i++) {
				a[i] = b[i];
			}
		}
	}

	public static float getDistance(float[] a, float[] b) {
		float dimention = a.length;
		if (a.length > b.length) {
			dimention = b.length;
		}
		float d = 0;
		for (int i = 0; i < dimention; i++) {
			d += (a[i] - b[i]) * (a[i] - b[i]);
		}
		return (float) Math.sqrt(d);
	}

}
