

import android.view.MotionEvent;
import android.view.View;

public class MySimpleTouchListener {

	public static final int[] NO_TOUCHING = new int[] { -1, -1 };
	public static int[] TOUCH_1 = new int[] { -1, -1 },
			TOUCH_2 = new int[] { -1, -1 };
	public static int pointersdown = 0;

	public static void dispatchTouchEvent(View v, MotionEvent e) {
		if (e.getAction() == MotionEvent.ACTION_DOWN
				|| e.getAction() == MotionEvent.ACTION_POINTER_DOWN) {
			pointersdown += 1;
			if (pointersdown == 1) {
				//Log.i("", "down" + 1);
				TOUCH_1 = new int[] { (int) e.getX(0), (int) e.getY(0) };
			}
		}
		if (e.getAction() == MotionEvent.ACTION_MOVE) {
			int total = e.getPointerCount();
			// Log.i("", "move: " + total);
			if (total == 2) {
				pointersdown = 2;
				TOUCH_2 = new int[] { (int) e.getX(1), (int) e.getY(1) };
				TOUCH_1 = new int[] { (int) e.getX(0), (int) e.getY(0) };
			} else if (total == 1) {
				pointersdown = 1;
				TOUCH_2 = new int[] { NO_TOUCHING[0], NO_TOUCHING[1] };
				TOUCH_1 = new int[] { (int) e.getX(0), (int) e.getY(0) };
			}
		}
		if (e.getAction() == MotionEvent.ACTION_UP
				|| e.getAction() == MotionEvent.ACTION_POINTER_UP) {
			int total = e.getPointerCount();
			pointersdown -= 1;
			// Log.i("", "up" + (pointersdown-1));
			if (total == 0) {
			} else if (pointersdown == 1) {
				TOUCH_2 = new int[] { NO_TOUCHING[0], NO_TOUCHING[1] };
				TOUCH_1 = new int[] { (int) e.getX(0), (int) e.getY(0) };
			} else if (pointersdown == 0) {
				TOUCH_1 = new int[] { NO_TOUCHING[0], NO_TOUCHING[1] };
			}
		}
		// Log.i("", "" + TOUCH_1[0] + "; " + TOUCH_1[1] + " ___ " + TOUCH_2[0]
		// + "; " + TOUCH_2[1] );
		//Log.i("", "pointersdown " + pointersdown);
	}
	public static boolean isTouching(){
		if(TOUCH_1[0] == NO_TOUCHING[0] && TOUCH_1[1] == NO_TOUCHING[1]){
			return false;
		}
		return true;
	}
}
