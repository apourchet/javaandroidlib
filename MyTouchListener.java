

import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class MyTouchListener {
	public static final String	tag			= "MyTouchListener";
	public static final Point	DEFAULT_NO_TOUCH	= new Point(123, 123);
	public static Point		CURRENT_DRAG_LENGTH	= new Point(DEFAULT_NO_TOUCH);
	public static Point		TOUCHING		= new Point(DEFAULT_NO_TOUCH);
	public static Point		DRAG_START		= new Point(DEFAULT_NO_TOUCH);
	public static Point		LAST_DRAG_END		= new Point(DEFAULT_NO_TOUCH);
	public static Point		LAST_DRAG_SPEED		= new Point(DEFAULT_NO_TOUCH);
	public static Point		LAST_DRAG_LENGHT	= new Point(DEFAULT_NO_TOUCH);
	public static Point		CURRENT_DRAG_SPEED	= new Point(DEFAULT_NO_TOUCH);//in pixel per second
	private static long		lastevent		= System.currentTimeMillis();

	public static void DispatchTouchEvent(View arg0, MotionEvent e) {
		long eventdelay = System.currentTimeMillis() - lastevent;
		if (e.getAction() == MotionEvent.ACTION_DOWN) {
			//Log.i(tag, "Touching: ACTION_DOWN");
			setPoint(DRAG_START, e);
			setPoint(TOUCHING, e);
		}
		if (e.getAction() == MotionEvent.ACTION_MOVE) {
			//Log.i(tag, "Touching: ACTION_MOVE");
			Point temp = new Point((int) e.getX(), (int) e.getY());
			CURRENT_DRAG_SPEED.x = (int) ((temp.x - TOUCHING.x)*1000 / (eventdelay));
			CURRENT_DRAG_SPEED.y = (int) ((temp.y - TOUCHING.y)*1000 / (eventdelay));

			setPoint(TOUCHING, e);
			CURRENT_DRAG_LENGTH.x = TOUCHING.x - DRAG_START.x;
			CURRENT_DRAG_LENGTH.y = TOUCHING.y - DRAG_START.y;
		}
		if (e.getAction() == MotionEvent.ACTION_UP) {
			//Log.i(tag, "Touching: ACTION_UP");
			setPoint(LAST_DRAG_END, e);
			LAST_DRAG_LENGHT.set(CURRENT_DRAG_LENGTH.x, CURRENT_DRAG_LENGTH.y);
			LAST_DRAG_SPEED.set(CURRENT_DRAG_SPEED.x, CURRENT_DRAG_SPEED.y);

			CURRENT_DRAG_SPEED=new Point(DEFAULT_NO_TOUCH);
			CURRENT_DRAG_LENGTH=new Point(DEFAULT_NO_TOUCH);
			DRAG_START=new Point(DEFAULT_NO_TOUCH);
			TOUCHING=new Point(DEFAULT_NO_TOUCH);
		}
		//displayVariables();
		lastevent = System.currentTimeMillis();
		//Log.i(tag, "EventDelay: " + eventdelay);
	}

	public static void setPoint(Point p, MotionEvent e) {
		p.set((int) e.getX(), (int) e.getY());
	}

	public static Point getCurrentDragLengh() {
		//returns the length of the current drag
		return new Point(CURRENT_DRAG_LENGTH);
	}

	public static Point getLastDragLenght() {
		//returns the length of the last drag
		return new Point(LAST_DRAG_LENGHT);
	}

	public static Point getLastDragPosition() {
		//returns the coordinates of the end of the last drag
		return new Point(LAST_DRAG_END);
	}

	public static Point getTouchingPoint() {
		//returns the coordinates of the current touch position
		return new Point(TOUCHING);
	}

	public static Point getCurrentDragSpeed() {

		return new Point(CURRENT_DRAG_SPEED);
	}

	public static void displayVariables() {
		Log.i(tag, "Variables: " + getCurrentDragLengh() + "; " + getCurrentDragSpeed() + "; " + getLastDragLenght() + "; " + getLastDragPosition() + "; " + getTouchingPoint());
	}

	public static double getDistance(Point a, Point b) {
		double distance = Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
		return distance;
	}
}
