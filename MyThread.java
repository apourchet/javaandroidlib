

public abstract class MyThread extends Thread {

	public boolean RUNNING = false;
	public abstract void run();
	
	public void setRunning(boolean b){
		RUNNING = b;
	}
	public boolean isRunning(){
		return RUNNING;
	}
	
}
