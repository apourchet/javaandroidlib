

public interface AsyncTaskResponseDispatcher {

	/**
	 * @return null if Internet connection cannot be established
	 * */
	public abstract void onResponseReturned(String returned);
	
}
