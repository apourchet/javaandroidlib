

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

public class MyAnimatedBitmap {
	
	public static final int FLAG_DRAW_AT_CENTER = 1;
	public static final String tag = "MyAnimatedBitmap";

	public Bitmap bitmap;
	public int total_cols, total_rows, mframe, frame_row, frame_col;
	public float width, height;
	public Rect source_rect;
	public int frame_delay;
	public Paint paint = new Paint();
	public long lastAnimTime;
	

	public MyAnimatedBitmap(Bitmap bitmap, int Columns, int Rows,
			int total_anim_time_ms) {
		this.bitmap = bitmap;
		width = bitmap.getWidth() / Columns;
		height = bitmap.getHeight() / Rows;
		paint.setAntiAlias(true);

		frame_delay = total_anim_time_ms / Columns;
		lastAnimTime = 0;
		total_cols = Columns;
		total_rows = Rows;
		mframe = 1;
		frame_row = 0;
		frame_col = 0;

		source_rect = new Rect();
		Log.e("MBITMAP", "");
	}

	public MyAnimatedBitmap(String bitmapname, int Columns, int Rows,
			int total_anim_time_ms) {
		bitmap = BitmapManager.getBitmap(bitmapname);
		width = bitmap.getWidth() / Columns;
		height = bitmap.getHeight() / Rows;
		paint.setAntiAlias(true);

		frame_delay = total_anim_time_ms / Columns;
		lastAnimTime = 0;
		total_cols = Columns;
		total_rows = Rows;
		mframe = 1;
		frame_row = 0;
		frame_col = 0;

		source_rect = new Rect();
		Log.e("MBITMAP", "");
	}

	public MyAnimatedBitmap(MyAnimatedBitmap MBMP) {
		bitmap = MBMP.bitmap;
		width = MBMP.bitmap.getWidth() / MBMP.total_cols;
		height = MBMP.bitmap.getHeight() / MBMP.total_rows;
		paint = MBMP.paint;

		frame_delay = MBMP.frame_delay;
		lastAnimTime = 0;
		total_cols = MBMP.total_cols;
		total_rows = MBMP.total_rows;
		mframe = 1;
		frame_row = 0;
		frame_col = 0;

		source_rect = new Rect();
		Log.e("MBITMAP", "");
	}

	public void doDraw(Canvas c, float[] pointdraw) {
		frame_col = mframe % total_cols;

		source_rect.set(frame_col * (int) width, frame_row * (int) height,
				(frame_col + 1) * (int) width,
				(frame_row * (int) height + (int) height));
		Rect drawRect = new Rect((int) pointdraw[0], (int) pointdraw[1],
				(int) pointdraw[0] + source_rect.width(), (int) pointdraw[1]
						+ source_rect.height());
		c.drawBitmap(bitmap, source_rect, drawRect, paint);
		if (System.currentTimeMillis() - lastAnimTime >= frame_delay) {
			mframe++;
			lastAnimTime = System.currentTimeMillis();
		}
	}
	public void doDraw(Canvas c, float[] pointdraw, int flag) {
		frame_col = mframe % total_cols;

		source_rect.set(frame_col * (int) width, frame_row * (int) height,
				(frame_col + 1) * (int) width,
				(frame_row * (int) height + (int) height));
		Rect drawRect = new Rect((int) pointdraw[0], (int) pointdraw[1],
				(int) pointdraw[0] + source_rect.width(), (int) pointdraw[1]
						+ source_rect.height());
		if(flag == FLAG_DRAW_AT_CENTER){
			drawRect = new Rect((int) (pointdraw[0]-(float)width/2), (int) (pointdraw[1] - (float)height/2),
					(int) (pointdraw[0] + source_rect.width() - (float)width/2), (int) (pointdraw[1]
							+ source_rect.height() - (float)height/2));
		}
		c.drawBitmap(bitmap, source_rect, drawRect, paint);
		if (System.currentTimeMillis() - lastAnimTime >= frame_delay) {
			mframe++;
			lastAnimTime = System.currentTimeMillis();
		}
	}

	public void setBitmapRow(int rowNumb) {	
		if (rowNumb + 1 <= total_rows) {
			frame_row = rowNumb;
		} else {
			Log.i("", "Could not switch Row");
		}
	}

	public void setAlpha(int a) {
		paint.setAlpha(a);
	}

	public void setBitmap(Bitmap b) {
		bitmap = b;
	}

	public void setBitmap(String bitmapname) {
		bitmap = BitmapManager.getBitmap(bitmapname);
	}
}
