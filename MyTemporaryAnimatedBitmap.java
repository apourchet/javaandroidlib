

import android.graphics.Canvas;

public class MyTemporaryAnimatedBitmap {

	public MyAnimatedBitmap BITMAP;
	public long CREATION_TIME, END_TIME;
	public float[] POSITION;
	public boolean isDrawing = true;
	/**This animates only once*/
	public MyTemporaryAnimatedBitmap(float[] position, String bitmapname, int Columns, int Rows, int total_anim_time_ms){
		BITMAP = new MyAnimatedBitmap(bitmapname, Columns, Rows, total_anim_time_ms);
		CREATION_TIME = System.currentTimeMillis();
		END_TIME = CREATION_TIME + total_anim_time_ms;
		POSITION = new float[]{position[0], position[1]};
	}
	public void doDraw(Canvas c){
		if(System.currentTimeMillis() > END_TIME){
			isDrawing = false;
		}else{
			BITMAP.doDraw(c, POSITION, MyAnimatedBitmap.FLAG_DRAW_AT_CENTER);
		}
	}
	
}
