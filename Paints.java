

import java.util.HashMap;

import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

public class Paints {

	public static boolean init = false;
	public static HashMap<Integer, Paint> PAINTS = new HashMap<Integer, Paint>();
	public static final void init(){
		Paint black= new Paint();
		black.setColor(Color.BLACK);
		PAINTS.put(Color.BLACK, black);
		
		Paint blue= new Paint();
		blue.setColor(Color.BLUE);
		PAINTS.put(Color.BLUE, blue);
		
		Paint red= new Paint();
		red.setColor(Color.RED);
		PAINTS.put(Color.RED, red);
		
		Paint cyan= new Paint();
		cyan.setColor(Color.CYAN);
		PAINTS.put(Color.CYAN, cyan);
		
		Paint yellow= new Paint();
		yellow.setColor(Color.YELLOW);
		PAINTS.put(Color.YELLOW, yellow);
		
		Paint gray= new Paint();
		gray.setColor(Color.GRAY);
		PAINTS.put(Color.GRAY, gray);
		
		Paint dkgray= new Paint();
		dkgray.setColor(Color.DKGRAY);
		PAINTS.put(Color.DKGRAY, dkgray);
		
		Paint white= new Paint();
		white.setColor(Color.WHITE);
		PAINTS.put(Color.WHITE, white);
		
		Paint green= new Paint();
		green.setColor(Color.GREEN);
		PAINTS.put(Color.GREEN, green);
		Log.e("", "Initialized paints");
		init = true;
	}
	public static final Paint paint(int color){
		if(init == false){
			init();
		}
		return PAINTS.get(color);
	}
}
