

import android.content.Context;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GenericSurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {

	private static final String tag = "GenericSurfaceView";
	public MyDrawnThread THREAD = null;
	private boolean isThreadSet = false;
	public int WIDTH, HEIGHT;
	public SurfaceHolder holder;

	public GenericSurfaceView(Context context, MyDrawnThread thread, int width, int height) {
		super(context);
		holder = getHolder();
		holder.addCallback(this);
		THREAD = thread;
		WIDTH = width;
		HEIGHT = height;
		isThreadSet = true;
	}

	public GenericSurfaceView(Context context, int width, int height) {
		super(context);
		holder = getHolder();
		holder.addCallback(this);
		WIDTH = width;
		HEIGHT = height;
		Log.i(tag, "New Aspect: " + WIDTH + "; " + HEIGHT);
	}

	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		Log.i("","Surface Changed");
	}

	public void surfaceCreated(SurfaceHolder arg0) {
		Log.i("","Surface Created");
		if (isThreadSet) {
			Log.i(tag, "Starting Thread");
			if(THREAD.isPaused()){
				THREAD.resumeUpdateDrawThread();
			}else{
				THREAD.setRunning(true);
				THREAD.start();
			}
		}
	}

	public void setThreadStart(MyDrawnThread thread) {
		Log.i(tag, "Starting Thread");
		THREAD = thread;
		THREAD.setRunning(true);
		THREAD.start();
		isThreadSet = true;
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		Log.i("","Surface Destroyed");
		boolean retry = true;
		THREAD.pauseUpdateDrawThread();
//		while (retry) {
//			try {
//				THREAD.join();
//				retry = false;
//			} catch (InterruptedException e) {
//			}
//		}
	}

}
