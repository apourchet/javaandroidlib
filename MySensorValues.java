

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.util.Log;

public class MySensorValues {

	public static int TYPE = Sensor.TYPE_ALL;
	public static float[] values = new float[3];
	public static void onSensorChanged(SensorEvent event) {
			values = event.values;
			Log.i("SensorValues", "values[1]*sign(value[2]): " + values[1]*MathCalcs.signOf(values[2]));
	}	
}
